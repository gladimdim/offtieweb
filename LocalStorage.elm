module LocalStorage exposing (DatabaseRecord, TimelineRecord)

type alias DatabaseRecord = {
    title: String
    , excerpt: String
    , content: String
    , url: String
    , time: Float
}

type alias TimelineRecord = {
    title: String,
    excerpt: String,
    key: String
}