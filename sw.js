//id #BUILD_UUID;
var CACHE_NAME="#BUILD_UUID";

var sameWorker = false;
self.addEventListener("install", function(event) {
	console.log("sw install");
    sameWorker = true;
	event.waitUntil(
	   caches.open(CACHE_NAME).then(function(cache) {
            return fetch("./precache.json").then(function(response) {
                return response.json().then(function(json) {
                    return cache.addAll(json);
                });
            });
       })
	);
});

self.addEventListener("fetch", function(event) {
    event.respondWith(
        fetch(event.request.clone()).catch(function() {
            console.log("sw fetched '" + event.request.url + "' from cache");
            return caches.match(event.request);
        }).then(function(response) {
            console.log("sw fetched '" + event.request.url + "' from internet");
            if (response.status === 200 && response.type === "basic") {
                caches.open(CACHE_NAME).then(function(cache) {
                    console.log("sw puts '" + event.request.url + "' to cache");
                    return cache.add(event.request.clone());
                });
            }
            return response;
        })
    );
});


self.addEventListener("activate", function(event) {
    console.log("sw activate");
    if (!sameWorker) {
        console.log("Removing cache");
        event.waitUntil(
            caches.keys().then(function(cacheNames) {
                return Promise.all([caches.delete(CACHE_NAME)]);
            })
        );
    }
});

if (!Cache.prototype.add) {
  Cache.prototype.add = function add(request) {
    return this.addAll([request]);
  };
}

if (!Cache.prototype.addAll) {
  Cache.prototype.addAll = function addAll(requests) {
    var cache = this;

    // Since DOMExceptions are not constructable:
    function NetworkError(message) {
      this.name = 'NetworkError';
      this.code = 19;
      this.message = message;
    }
    NetworkError.prototype = Object.create(Error.prototype);

    return Promise.resolve().then(function() {
      if (arguments.length < 1) throw new TypeError();

      // Simulate sequence<(Request or USVString)> binding:
      var sequence = [];
      var counter = 0;
      requests = requests.map(function(request) {
        console.log(counter++);
        if (request instanceof Request) {
          return request;
        }
        else {
          return String(request); // may throw TypeError
        }
      });

      return Promise.all(
        requests.map(function(request) {
          if (typeof request === 'string') {
            request = new Request(request);
          }

          var scheme = new URL(request.url).protocol;

          if (scheme !== 'http:' && scheme !== 'https:') {
            throw new NetworkError("Invalid scheme");
          }

          return fetch(request.clone());
        })
      );
    }).then(function(responses) {
      // TODO: check that requests don't overwrite one another
      // (don't think this is possible to polyfill due to opaque responses)
      return Promise.all(
        responses.map(function(response, i) {
          return cache.put(requests[i], response);
        })
      );
    }).then(function() {
      return undefined;
    });
  };
}
