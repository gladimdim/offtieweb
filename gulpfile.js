/*
Copyright (c) 2015 The Polymer Project Authors. All rights reserved.
This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
Code distributed by Google as part of the polymer project is also
subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
*/

'use strict';

// Include Gulp & Tools We'll Use
var gulp = require('gulp');
var glob = require('glob');
var path = require('path');
var fs = require('fs');
var Promise = require("bluebird");
var fsecopy = Promise.promisify(require("fs-extra").copy);
var fse = require("fs-extra");
var autoprefixer = require("gulp-autoprefixer");
var uuid = require("node-uuid");
var replace = require("gulp-replace");
var minify = require("gulp-minify");
var elm = require("gulp-elm");

gulp.task('precache', function (callback) {
  var dir = './';
  glob('{libs,styles}/**/*.*', {cwd: dir}, function(error, files) {
    if (error) {
      callback(error);
    } else {
      files.push("index.html", "elm-min.js");
      console.log(files);
      var filePath = path.join(dir, 'precache.json');
      fs.writeFile(filePath, JSON.stringify(files), callback);
    }
  });
});

gulp.task("build", ["minify"], function(callback) {
  fse.emptyDirSync("./dist");
  var copyPromises = [];
  var aFiles = [
    "index.html",
    "elm-min.js",
    "precache.json",
    "sw.js",
    "styles",
    "favicons"
  ];
  copyPromises = aFiles.map(function(el) {
    return createfseCopy(el);
  });
  return Promise.all(copyPromises).then(function(res) {
    console.log("yes");
  }).catch(function(er) {
    console.error(er);
  });
});

gulp.task("prefix", ["build"], function() {
  return gulp.src("./styles/styles.css")
    .pipe(autoprefixer({
      browsers: ["last 2 versions"],
      cascade: false
    }))
    .pipe(gulp.dest("dist/styles"));
});

gulp.task("watch", function() {
  gulp.watch("./index.html", ["default"]);
  gulp.watch("./styles/*.css", ["default"]);
  gulp.watch("**/*.elm", ["default"]);
  gulp.watch("./elm.js", ["default"]);
});

gulp.task("watch-elm", function() {
    gulp.watch("**/*.elm", ["default"]);
});

gulp.task("build-elm", function() {
  return gulp.src("./**.elm")
    .pipe(elm.bundle("elm.js"))
    .on('error', function(e) {
      console.log(e);
    })
    .pipe(gulp.dest("./"));
});

gulp.task("update-uuid", ["build"], function() {
    return gulp.src("./sw.js")
    .pipe(replace("#BUILD_UUID", uuid.v1()))
    .pipe(gulp.dest("./dist/"));

});

gulp.task("minify", ["build-elm"], function() {
    return gulp.src("./elm.js")
    .pipe(minify())
    .pipe(gulp.dest("./"));
});

function createfseCopy(s) {
    console.log("copying: " + s);
    return fsecopy("./" + s, "./dist/" + s);
}

gulp.task("default", ["build-elm", "minify", "precache", "prefix", "build", "update-uuid"], function() {
  console.log("Elmed, minified, Precached, prefixed and built");
});
