port module Offtie exposing (..)

import Html exposing (..)
import Html.Events exposing (..)
import Html.Attributes exposing (..)
import Html.App as Html
import Http
import Json.Decode as Json exposing (Decoder, (:=), string)
import Task
import Time
import LocalStorage exposing (DatabaseRecord, TimelineRecord)
import Json.Encode exposing (..)
import Material
import Material.Button as Button exposing (..)
import Material.Textfield as Textfield exposing (..)
import Material.Card as Card exposing (..)
import Material.Options as Options exposing (cs, css)
import Material.Helpers exposing (map1st, map2nd)
import Material.Color
import Material.Progress as Loading exposing (..)
import List exposing (isEmpty)
import Material.Snackbar as Snackbar
import Maybe exposing (..)
import Material.Grid exposing (grid, cell, size, Device(..))
import Dom.Scroll exposing (..)
import Material.Grid exposing (size, cell, grid, Device(..))

main: Program Never
main =
    Html.program
    { init = init
    , update = update
    , view = view
    , subscriptions = subscriptions}

{-- OUT PORTS (INTO JS) --}
port urlLoadedPort : DatabaseRecord -> Cmd msg
port getTimeline : Int -> Cmd msg
port getArticlePort: String -> Cmd msg
port removeTimeline: Int -> Cmd msg

{-- IN PORTS (INTO ELM) --}
port articlesInput: (DatabaseRecord -> msg) -> Sub msg
port timeline : (List TimelineRecord -> msg) -> Sub msg

subscriptions : Model -> Sub Msg
subscriptions model = Sub.batch [timeline RenderTimeline, articlesInput ShowArticle]

{----INIT----}
init : (Model, Cmd Msg)
init  = (model, Cmd.none)

{---- MODEL ----}
type alias Model = {
    url: String
    ,timeline: List TimelineRecord
    ,mdl : Material.Model
    ,selectedRecord : Maybe DatabaseRecord
    ,snackbar : Snackbar.Model Bool
    ,loadingFlag : Bool
}

model : Model
model =
  {url = "https://www.blockloop.io/mastering-bash-and-terminal", timeline = [], mdl = Material.model, selectedRecord = Maybe.Nothing, snackbar = Snackbar.model, loadingFlag = False}

type alias Mdl =
    Material.Model

{---- ACTION ----}
type Msg
    = ShowArticle DatabaseRecord
    | ShowContent
    | RemoveAll
    | UpdateUrl String
    | DownloadUrl String
    | FetchSucceed DatabaseRecord
    | FetchFail Http.Error
    | RenderTimeline (List TimelineRecord)
    | GetArticle String
    | Mdl (Material.Msg Msg)
    | ClearUrl
    | Snackbar (Snackbar.Msg Bool)
    | StopProgressBar
    | StartProgressBar
    | None
    | GetTimeline

update: Msg -> Model -> (Model, Cmd Msg)
update msg model =
    case msg of
        ShowArticle record -> ({model | selectedRecord = Maybe.Just record}, Cmd.none)
        ShowContent ->
            {model | selectedRecord = Maybe.Nothing} !
            [ Task.perform (\_ -> None) (\_ -> Debug.log "dd" None) (Dom.Scroll.toTop "bodyId") ]
        RemoveAll -> ({model | selectedRecord = Maybe.Nothing}, removeTimeline 1)
        UpdateUrl str -> ({model | url = str}, Cmd.none)
        DownloadUrl str -> (model, Cmd.batch[urlDownloader str, setProgressBar True])
        FetchSucceed dbrecord -> (model, Cmd.batch[urlLoadedPort <| dbrecord, setProgressBar False])
        FetchFail error -> notify "Error. Check URL" model
        RenderTimeline t -> ({model | timeline = t}, Cmd.none)
        GetArticle articleKey -> (model, getArticlePort <| articleKey)
        Mdl msg' -> Material.update msg' model
        ClearUrl -> ({model | url = ""}, Cmd.none)
        Snackbar action' ->
            Snackbar.update action' model.snackbar
               |> map1st (\s -> { model | snackbar = s })
               |> map2nd (Cmd.map Snackbar)
        StartProgressBar -> ({model | loadingFlag = True}, Cmd.none)
        StopProgressBar -> ({model | loadingFlag = False}, Cmd.none)
        None -> (model, Cmd.none)
        GetTimeline -> (model, getTimeline 1)

{---- VIEW ----}
view: Model -> Html Msg
view model =
    div [id "newApp"] [
        div [class "button-container"] [
            div [Html.Attributes.classList [
                    ("header-block", True),
                    ("timelineButtons", True),
                    ("btn-group", True)]
                ]
                [
                Textfield.render Mdl [0] model.mdl [
                    Textfield.label "Enter URL"
                    ,Textfield.onInput UpdateUrl
                    ,Textfield.autofocus
                    ,Textfield.floatingLabel
                    ,Textfield.value model.url
                ]
                ,Button.render Mdl [0] model.mdl [Button.raised, Button.colored, Button.ripple, Button.onClick (DownloadUrl model.url)] [ Html.text "Save"]
                ,Button.render Mdl [1] model.mdl [Button.raised, Button.ripple, Button.onClick ClearUrl] [Html.text "Clear"]
                ,Button.render Mdl [2] model.mdl [Button.raised, Button.ripple, Button.onClick ShowContent] [Html.text "Show Saved"]
                ,Button.render Mdl [3] model.mdl [Button.primary, Button.raised, Button.colored, Button.ripple, Button.onClick RemoveAll] [ Html.text "Remove All"]
            ]
            ,if model.loadingFlag then Loading.indeterminate
            else div [] []
        ]
        ,renderBody model.timeline model.selectedRecord
        ,div [] [renderArticleContent model.selectedRecord]
        ,Snackbar.view model.snackbar |> Html.map Snackbar
    ]
width100Style: Attribute a
width100Style = Html.Attributes.style [("width", "100%")]

setProgressBar: Bool -> Cmd Msg
setProgressBar flag =
    case flag of
        True -> Task.perform (\_ -> Debug.crash "Lol") identity (Task.succeed StartProgressBar)
        False -> Task.perform (\_ -> Debug.crash "Lol") identity (Task.succeed StopProgressBar)

notify : String -> Model -> (Model, Cmd Msg)
notify mes model =
  let
    toast = (Snackbar.toast True mes)
    (snackbar', effect) =
      Snackbar.add toast model.snackbar
        |> map2nd (Cmd.map Snackbar)
    model' =
      { model | snackbar = snackbar' }
  in
    ( model', Cmd.batch [ effect, setProgressBar False ])

{---- NETWORK ----}
urlDownloader: String -> Cmd Msg
urlDownloader url =
    Task.perform FetchFail FetchSucceed
    (
        (Http.send Http.defaultSettings (postJsonRequest url) |> Http.fromJson decodeUrl)
        `Task.andThen`
        (\response -> Task.map (\time -> {response | time = Time.inMilliseconds time}) Time.now)
    )


decodeUrl =
          Json.object4 (\a b c d -> {content = a, excerpt = b, title = c, url = d, time = 0.0})
                       ("content" := Json.string)
                       ("excerpt" := Json.string)
                       ("title" := Json.string)
                       ("url" := Json.string)

postJsonRequest :  String -> Http.Request
postJsonRequest url =
    { verb = "POST"
        , headers = [
          ("Content-Type", "application/json")
          , ("Access-Control-Request-Method", "POST")
        ]
        , url = "https://offtie.com:3000/geturl"
        , body = (Http.string ("{\"url\": \"" ++ url ++ "\"}"))
    }

{---- RENDER HELPERS----}
renderBody: List TimelineRecord -> Maybe DatabaseRecord -> Html Msg
renderBody timeline selectedRecord =
    if (List.isEmpty timeline) then
        div [] [renderAbout "1"]
    else
        case selectedRecord of
            Nothing ->
                grid [] (List.map renderTimelineElement timeline)
            Just value ->
                Html.text ""

renderAbout: String -> Html msg
renderAbout str =
    Card.view [
        css "width" "100%"
        ,Material.Color.background (Material.Color.color Material.Color.Yellow Material.Color.A700)
    ]
    [
        Card.title [] [
            Card.head [] [Html.text "Save any URL to read later offline without ads"]
        ],
        Card.text [Material.Color.text Material.Color.black] [
                Html.text "Offtie allows you to read saved articles offline. "
                ,Html.text "Just paste URL, press SAVE button and return back to site to read it offline when you need. "
                ,Html.text "Offtie does not save anything on server. Data stays in your Browser. "
                ,Html.text "Offtie uses www.readability.com API to get only content of article"
            ]
    ]

renderTimelineElement: TimelineRecord -> Material.Grid.Cell Msg
renderTimelineElement rec =
    let white =
        Material.Color.text Material.Color.white
    in
        cell [ Material.Grid.size Tablet 4, Material.Grid.size Desktop 4, Material.Grid.size Phone 12] [
            Card.view
            [
                css "width" "100%"
                ,Material.Color.background (Material.Color.color Material.Color.Green Material.Color.S500)
                ,Options.attribute <| Html.Events.onClick (GetArticle rec.key)
            ]
            [
                Card.title [] [
                    Card.head [ white ] [Html.text rec.title]
                    ,Card.subhead [white] [Html.text rec.excerpt]
                ]
            ]
        ]

renderArticleContent: Maybe DatabaseRecord -> Html Msg
renderArticleContent record =
    case record of
        Maybe.Just value ->
            Card.view
            [
                css "width" "100%"
                ,Material.Color.background (Material.Color.color Material.Color.LightBlue Material.Color.S50)
                ,Options.attribute <| Html.Events.onClick ShowContent
            ]
            [
                Card.title [] [
                    Card.head [] [ Html.text value.title]
                    ,Card.subhead [] [ Html.text value.url]
                ]
                ,Card.text [] [
                    div [class "content", property "innerHTML" <| Json.Encode.string value.content] []
                ]
            ]
        Maybe.Nothing -> div [] []
